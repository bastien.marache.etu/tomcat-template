#!/bin/bash

tpPath=$TOMCATPATH/webapps/$1

# public="index.html index.jsp style.css login.html"
public=$(cat $tpPath/main/config/public)
vue=$(cat $tpPath/main/config/vue)


javac -d $tpPath/WEB-INF/classes -cp $CLASSPATH:.:$tpPath/WEB-INF/classes $tpPath/main/src/**/*.java
javac -d $tpPath/WEB-INF/classes -cp $CLASSPATH:.:$tpPath/WEB-INF/classes $tpPath/main/src/*.java

cp $tpPath/main/front/pages/*.html $tpPath/WEB-INF
cp $tpPath/main/front/css/*.css $tpPath/WEB-INF
cp $tpPath/main/front/imgs/* $tpPath/WEB-INF

cp $tpPath/main/jsp/*.jsp $tpPath/WEB-INF

cp $tpPath/main/src/util/config.prop $tpPath/WEB-INF/classes/util/

# Les $public vont à l'ext de WEB-INF
for val in $public; do
    if [ -f "$tpPath/WEB-INF/$val" ]; then
        mv "$tpPath/WEB-INF/$val" "$tpPath"
    fi
done

# Les $vue vont dans WEB-INF/vue (voir MVC tp321)
for val in $vue; do
    if [ -f "$tpPath/WEB-INF/$val" ]; then
        if [ -d "$tpPath/WEB-INF/vue" ]; then
            mv "$tpPath/WEB-INF/$val" "$tpPath/WEB-INF/vue"
        else
            mkdir "$tpPath/WEB-INF/vue"
            mv "$tpPath/WEB-INF/$val" "$tpPath/WEB-INF/vue"
        fi
    fi
done