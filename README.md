# **tomcat-template**

## Un système simple de templating pour un serveur apache tomcat

## **Choses à faire**

- Ajouter la variable d'environnement `TOMCATPATH` de la manière suivante

```sh
# ~/.bashrc

...
export TOMCATPATH="Le chemin vers le répretoire tomcat ex: ~/Documents/tomcat"
```

- Dans le script `iniPart.sh`, modifier le chemin vers le template

- Pour pouvoir utiliser la classe `Database`, il faut créer un fichier `config.prop` contenant les identifiants de connexion à postgres. Par exemple :

```conf
# .../tomcat/webapps/{NomDuProjet}/main/src/util/config.prop

driver=org.postgresql.Driver
url=jdbc:postgresql://localhost/{NomDeLaBaseDeDonnées}
login={Login}
password={MotDePasse}
```