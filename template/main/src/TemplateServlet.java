import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.AbstractServlet;
import util.HtmlGenerator;

@WebServlet("/servlet-Template")
public class TemplateServlet extends AbstractServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html;charset=UTF-8");
        PrintWriter out = res.getWriter();
        out.println("<!doctype html>");
        out.println(HtmlGenerator.head("Title of the servlet"));

        StringBuilder body = new StringBuilder();

        body.append(HtmlGenerator.tag("h1", "Title of the servlet"));

        // Add things here

        out.println(HtmlGenerator.tag("body", HtmlGenerator.tag("center", body.toString())));
    }

    // Uncomment if usefull

    // @Override
    // protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    //     // TODO Auto-generated method stub
    //     super.doGet(req, resp);
    // }

    // @Override
    // protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    //     // TODO Auto-generated method stub
    //     super.doPost(req, resp);
    // }
    
}
