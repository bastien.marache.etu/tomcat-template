package util;

import javax.servlet.http.HttpServlet;

public abstract class AbstractServlet extends HttpServlet {
    public boolean isAvailable(String param) {
        if (param == null || param.isEmpty() || param.isBlank()) return false;
        return true;
    }
}
