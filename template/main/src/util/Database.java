package util;

import java.sql.Statement;
import java.util.Properties;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {

    private Connection con = null;

    private String driver = null;
    private String url = null;
    private String login = null;
    private String password = null;

    public Database() {
        Properties p = new Properties();
        try {
            p.load(getClass().getResourceAsStream("config.prop"));
            driver = p.getProperty("driver");
            url = p.getProperty("url");
            login = p.getProperty("login");
            password = p.getProperty("password");
        } catch (IOException e) {
            System.out.println("NULLLLL");
        }
    }

    public Connection getConnection() {
        
        if (con == null) {
            try {
                Class.forName(driver);
                con = DriverManager.getConnection(url, login, password);
            } catch (SQLException | ClassNotFoundException e) {
                System.out.println("Failed to get connection: " + e.getMessage()); 
            }

            return con;
        }
        
        return con;
    }

    public void closeConnection() {
        if (con != null) {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                System.out.println("Failed to close connection: " + ex.getMessage());
            }
        } else {
            System.out.println("Connection is already closed");
        }
    }

    public int runRequest(String request) {
        try {
            Connection connection = this.getConnection();
            Statement  statement  = connection.createStatement();
            int        result     = statement.executeUpdate(request);
            connection.close();
            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public ResultSet executeQuery(String request) {
        ResultSet result = null;
        try {
            Connection connection = this.getConnection();
            Statement  statement  = connection.createStatement();
            result = statement.executeQuery(request);
            connection.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

}
