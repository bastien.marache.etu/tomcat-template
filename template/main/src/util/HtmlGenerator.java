package util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class HtmlGenerator {
    public static String tag(String tag, String content) {
        return "<" + tag + ">" + content + "</" + tag + ">";
    }

    public static String input(String type, String name, String placeholder, String value) {
        return "<input type=\""+type+"\" name=\""+name+"\" placeholder=\""+placeholder+"\" value=\""+value+"\">";
    }

    public static String hiddenInput(String name, String value) {
        return "<input type=\"hidden\" name=\""+name+"\" value=\""+value+"\">";
    }

    public static String brook() {
        return "<br>";
    }

    public static String form(String action, String method, String content) {
        return "<form action=\""+action+"\" method=\""+method+"\">"+content+"</form>";
    }

    public static String button(String type, String content) {
        return "<button type=\""+type+"\">"+content+"</button>";
    }

    public static String link(String href, String content) {
        return "<a href=\"\">"+content+"</a>";
    }

    public static String head(String title) {
        StringBuilder body = new StringBuilder();

        body.append("<meta charset=\"UTF-8\">");
        body.append("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
        body.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
        body.append("<link rel=\"stylesheet\" href=\"./style.css\">");
        body.append(tag("title", title));

        return tag("head", body.toString());
    }

    public static String generateTable(ResultSet resultSet, boolean generateHeader) {
        try {
            ResultSetMetaData rsmd = resultSet.getMetaData();

            StringBuilder result = new StringBuilder("<table>");

            if (generateHeader) {
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    result.append(tag("th", rsmd.getColumnName(i + 1)));
                }
            }

            while (resultSet.next()) {
                result.append("<tr>");

                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    result.append(tag("td", resultSet.getString(i + 1)));
                }

                result.append("</tr>");
            }

            result.append("</table");
            return result.toString();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        }
    }
}
