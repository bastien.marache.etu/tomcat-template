drop table if exists template;

create table template(
    id serial primary key,
    login text,
    password text
);

insert into template(login, password) values('first.person.etu', 'thisisafirstpassword');
insert into template(login, password) values('second.person.etu', 'thisisasecondpassword');
insert into template(login, password) values('third.person.etu', 'thisisathirdpassword');